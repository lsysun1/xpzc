//
//  XP_PeopleSearchView.h
//  XP
//
//  Created by 李松玉 on 15/4/17.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XP_PeopleSearchView : UITextField

+ (instancetype)searchBar;

@end
