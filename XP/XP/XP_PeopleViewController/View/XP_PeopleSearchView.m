//
//  XP_PeopleSearchView.m
//  XP
//
//  Created by 李松玉 on 15/4/17.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "XP_PeopleSearchView.h"

@implementation XP_PeopleSearchView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.font = [UIFont boldSystemFontOfSize:17];
        self.placeholder = @"搜索";
        self.background = [UIImage imageNamed:@"lb_bg"];
        
        
        self.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 0)];
        self.leftView.userInteractionEnabled = NO;
        self.leftViewMode = UITextFieldViewModeAlways;
        
        // Text 垂直居中
        self.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        
        
        // 通过init来创建初始化绝大部分控件，控件都是没有尺寸
        UIImageView *searchIcon = [[UIImageView alloc] init];
        searchIcon.frame = CGRectMake(20, 8, 24, 26);
        searchIcon.image = [UIImage imageNamed:@"ss"];
        [self addSubview:searchIcon];
    }
    return self;
}

+ (instancetype)searchBar
{
    return [[self alloc] init];
}

@end
