//
//  XP_PeopleViewController.m
//  XP
//
//  Created by 李松玉 on 15/4/17.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "XP_PeopleViewController.h"
#import "XP_PeopleSearchView.h"

@interface XP_PeopleViewController () <UITableViewDataSource,UITableViewDelegate>
{
    XP_PeopleSearchView *_searchBar;
    UITableView *_tableView;
}
@end

@implementation XP_PeopleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"附近的人";
    [self addBackBtn];
//    [self setUpSearchBar];
    [self setUpTableView];
}



- (void) setUpSearchBar
{
    _searchBar = [[XP_PeopleSearchView alloc]initWithFrame:CGRectMake(0, 64 + 10, ScreenWidth, 40)];
    [self.view addSubview:_searchBar];
}


- (void) setUpTableView
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - CGRectGetMaxY(_searchBar.frame))];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 80)];
    headView.backgroundColor = UIColorFromRGB(0xf1f1f1);
    
    _searchBar = [[XP_PeopleSearchView alloc]initWithFrame:CGRectMake(0, 20, ScreenWidth, 40)];
    [headView addSubview:_searchBar];
    
    _tableView.tableHeaderView = headView;
    [self.view addSubview:_tableView];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    cell.textLabel.text = @"123123";
    return cell;
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
