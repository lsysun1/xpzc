//
//  XP_BaseViewController.m
//  XP
//
//  Created by 李松玉 on 15/4/15.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "XP_BaseViewController.h"

@interface XP_BaseViewController ()

@end

@implementation XP_BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xf1f1f1);
    [self setStatusBarBgColor];
}



- (void) addBackBtn
{
    [self.navigationItem setHidesBackButton:YES];
    
    UIButton *backBtn = [[UIButton alloc]initWithFrame:CGRectMake(0,0,30,30)];
    [backBtn setImage:[UIImage imageNamed:@"nav_back"]forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barBackBtn = [[UIBarButtonItem alloc]initWithCustomView:backBtn];
    
    self.navigationItem.leftBarButtonItem = barBackBtn;
}

- (void) setStatusBarBgColor
{
    UIView *statusBarView=[[UIView alloc] initWithFrame:CGRectMake(0,-20, ScreenWidth, 20)];
    statusBarView.backgroundColor=[UIColor orangeColor];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    
    [self.navigationController.navigationBar addSubview:statusBarView];
}


- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
