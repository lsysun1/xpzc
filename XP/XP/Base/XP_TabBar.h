//
//  XP_TabBar.h
//  XP
//
//  Created by 李松玉 on 15/4/8.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <UIKit/UIKit.h>

@class XP_TabBar;

// 因为XP_TabBar继承自UITabBar，所以称为XP_TabBar的代理，也必须实现UITabBar的代理协议
@protocol XP_TabBarDelegate <UITabBarDelegate>
@optional
- (void)tabBarDidClickPlusButton:(XP_TabBar *)tabBar;
@end

@interface XP_TabBar : UITabBar

@property (nonatomic, weak) id<XP_TabBarDelegate> delegate;

@end
