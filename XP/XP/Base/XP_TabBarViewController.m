//
//  XP_TabBarViewController.m
//  XP
//
//  Created by 李松玉 on 15/4/8.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "XP_TabBarViewController.h"
#import "XP_NavigationViewController.h"
#import "XP_HomeViewController.h"
#import "XP_MailViewController.h"
#import "XP_MessageViewController.h"
#import "XP_ProfileViewController.h"
#import "XP_TabBar.h"
#import "XP_PeopleViewController.h"


@interface XP_TabBarViewController ()<XP_TabBarDelegate>
{
    UIView *_plusView;
    UIButton *_postBtn;
    UIButton *_renBtn;
}
@end

@implementation XP_TabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 1.初始化子控制器
    XP_HomeViewController *homeVC = [[XP_HomeViewController alloc]init];
    [self addChildVc:homeVC title:@"首页" image:@"sy_sy_off" selectedImage:@"sy_sy_on"];

    XP_MailViewController *mailVC = [[XP_MailViewController alloc]init];
    [self addChildVc:mailVC title:@"商城" image:@"sy_sc_off" selectedImage:@"sy_sc_on"];

    
    XP_MessageViewController *messageVC = [[XP_MessageViewController alloc]init];
    [self addChildVc:messageVC title:@"消息" image:@"syxx_off" selectedImage:@"syxx_on"];


    XP_ProfileViewController *profileVC = [[XP_ProfileViewController alloc]init];
    [self addChildVc:profileVC title:@"个人中心" image:@"sy_grzx_off" selectedImage:@"sy_grzx_on"];


    
    // 2.更换系统自带的tabbar
    XP_TabBar *tabBar = [[XP_TabBar alloc] init];
    tabBar.delegate = self;
    [self setValue:tabBar forKeyPath:@"tabBar"];
    
}


/**
 *  添加一个子控制器
 *
 *  @param childVc       子控制器
 *  @param title         标题
 *  @param image         图片
 *  @param selectedImage 选中的图片
 */
- (void)addChildVc:(UIViewController *)childVc title:(NSString *)title image:(NSString *)image selectedImage:(NSString *)selectedImage
{
    // 设置子控制器的文字
    childVc.title = title; // 同时设置tabbar和navigationBar的文字
    
    // 设置子控制器的图片
    childVc.tabBarItem.image = [UIImage imageNamed:image];
    childVc.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImage]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    // 设置文字的样式
    NSMutableDictionary *textAttrs = [NSMutableDictionary dictionary];
//    textAttrs[NSForegroundColorAttributeName] = HWColor(123, 123, 123);
    NSMutableDictionary *selectTextAttrs = [NSMutableDictionary dictionary];
    selectTextAttrs[NSForegroundColorAttributeName] = [UIColor orangeColor];
    [childVc.tabBarItem setTitleTextAttributes:textAttrs forState:UIControlStateNormal];
    [childVc.tabBarItem setTitleTextAttributes:selectTextAttrs forState:UIControlStateSelected];
    //    childVc.view.backgroundColor = HWRandomColor;
    
    // 先给外面传进来的小控制器 包装 一个导航控制器
//    XP_NavigationViewController *nav = [[XP_NavigationViewController alloc] initWithRootViewController:childVc];
    // 添加为子控制器
    [self addChildViewController:childVc];
}


- (void) addPlusView
{
    _plusView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    _plusView.backgroundColor = [UIColor blackColor];
    _plusView.alpha = 0.5;
    UITapGestureRecognizer*tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(plusViewDidClick)];
    [_plusView addGestureRecognizer:tapGesture];
    [self.view addSubview:_plusView];
    
    _postBtn = [[UIButton alloc]initWithFrame:CGRectMake(70, ScreenHeight - 150, 80, 80)];
    [_postBtn setBackgroundImage:[UIImage imageNamed:@"sy_ft"] forState:UIControlStateNormal];
    [_postBtn setBackgroundImage:[UIImage imageNamed:@"sy_ft"] forState:UIControlStateHighlighted];
    _postBtn.backgroundColor = [UIColor redColor];
    [self.view addSubview:_postBtn];
    
    _renBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 150, ScreenHeight - 150, 80, 80)];
    [_renBtn setBackgroundImage:[UIImage imageNamed:@"sy_fjdr"] forState:UIControlStateNormal];
    [_renBtn setBackgroundImage:[UIImage imageNamed:@"sy_fjdr"] forState:UIControlStateHighlighted];
    _renBtn.backgroundColor = [UIColor greenColor];
    [_renBtn addTarget:self action:@selector(renBtnDidClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view  addSubview:_renBtn];
}

- (void) plusViewDidClick
{
    [_plusView removeFromSuperview];
    [_postBtn removeFromSuperview];
    [_renBtn removeFromSuperview];
}


- (void) renBtnDidClick
{
    XP_PeopleViewController *peopleVC = [[XP_PeopleViewController alloc]init];
    [self.navigationController pushViewController:peopleVC animated:YES];
}


#pragma mark - XP_TabBar Delegate Method
- (void)tabBarDidClickPlusButton:(XP_TabBar *)tabBar
{
    [self addPlusView];
}


@end
