//
//  XP_NavigationViewController.h
//  XP
//
//  Created by 李松玉 on 15/4/8.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <UIKit/UIKit.h>


//window窗口
#define WINDOW  [[UIApplication sharedApplication]keyWindow]


@interface XP_NavigationViewController : UINavigationController

// Enable the drag to back interaction, Defalt is YES.
@property (nonatomic,assign) BOOL canDragBack;

@end
