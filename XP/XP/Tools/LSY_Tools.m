//
//  LSY_Tools.m
//  Tools
//
//  Created by 李松玉 on 15/3/16.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "LSY_Tools.h"

@implementation LSY_Tools

#pragma mark 创建UILabel
+ (UILabel *)createLabel:(CGRect)frame andTextColor:(UIColor *)color
                 andText:(NSString *)text andFont:(UIFont *)font
                andAlign:(NSTextAlignment)align Tag:(int)tag
{
    UILabel *nameLabel = [[UILabel alloc] init];
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.text = text;
    nameLabel.textColor = color;
    nameLabel.tag = tag;
    nameLabel.font = font;
    nameLabel.frame = frame;
    nameLabel.textAlignment = align;
    return nameLabel;
}

#pragma mark 创建UIView
+ (UIView *)createView:(CGRect)frame BackColor:(UIColor *)color Tag:(int)tag
{
    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.backgroundColor = color;
    view.tag = tag;
    return view;
}

#pragma mark 创建UIButton
+ (UIButton *)createButton:(CGRect)frame Title:(NSString *)title
                TitleColor:(UIColor *)color Bgimage:(NSString *)imgname
                       Tag:(int)tag Font:(UIFont *)font
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:color forState:UIControlStateNormal];
    [btn.titleLabel setFont:font];
    btn.frame = frame;
    [btn setBackgroundImage:[UIImage imageNamed:imgname] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_pressed",imgname]] forState:UIControlStateHighlighted];
    btn.tag = tag;
    return btn;
}

#pragma mark --计算字符空间大小
+ (CGSize)sizeWithString:(NSString *)string font:(UIFont *)font width:(NSInteger)width
{
    CGRect rect = [string boundingRectWithSize:CGSizeMake(width, MAXFLOAT)//显示的最大容量
                                       options: NSStringDrawingUsesLineFragmentOrigin //描述字符串的附加参数
                                    attributes:@{NSFontAttributeName: font}//描述字符串的参数
                                       context:nil];//上下文
    //返回值
    return rect.size;
}



@end
