//
//  LSY_Tools.h
//  Tools
//
//  Created by 李松玉 on 15/3/16.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>

@interface LSY_Tools : NSObject

#pragma mark - UI相关
/**
 *  生成UILabel
 *
 *  @param frame label frame
 *  @param color 字体颜色
 *  @param text  文本内容
 *  @param font  字体大小
 *  @param align 对齐方式
 *  @param tag   tag
 *
 *  @return UILabel
 */
+ (UILabel *)createLabel:(CGRect)frame andTextColor:(UIColor *)color
                 andText:(NSString *)text andFont:(UIFont *)font
                andAlign:(NSTextAlignment)align Tag:(int)tag;

/**
 *  生成UIView
 *
 *  @param frame view frame
 *  @param color 背景颜色
 *
 *  @return UIView
 */
+ (UIView *)createView:(CGRect)frame BackColor:(UIColor *)color Tag:(int)tag;

/**
 *  生成UIButton
 *
 *  @param frame   button frame
 *  @param title   标题
 *  @param color   标题颜色
 *  @param imgname 背景图片
 *
 *  @return UIButton
 */
+ (UIButton *)createButton:(CGRect)frame Title:(NSString *)title
                TitleColor:(UIColor *)color Bgimage:(NSString *)imgname
                       Tag:(int)tag Font:(UIFont *)font;


/**
 *  计算字符空间大小
 */
+ (CGSize)sizeWithString:(NSString *)string font:(UIFont *)font width:(NSInteger)width;









@end
