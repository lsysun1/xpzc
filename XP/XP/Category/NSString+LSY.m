//
//  NSString+LSY.m
//  Tools
//
//  Created by 李松玉 on 15/3/16.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "NSString+LSY.h"

@implementation NSString (LSY)

#pragma mark 清空字符串中的空白字符
- (NSString *)trimString
{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

#pragma mark 是否空字符串
- (BOOL)isEmptyString
{
    return (self == nil || self.length == 0);
}

#pragma mark 返回沙盒中的文件路径
- (NSString *)documentsPath
{
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    return [path stringByAppendingString:self];
}

#pragma mark 写入系统偏好
- (void)saveToNSDefaultsWithKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setObject:self forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark 时间戳转NSString
- (NSString *)dateSince1970:(NSString *)dateline format:(NSString *)format
{
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = format;
    NSDate *createdDate = [NSDate dateWithTimeIntervalSince1970:[dateline doubleValue]];
    return [fmt stringFromDate:createdDate];
}


@end
