//
//  NSString+LSY.h
//  Tools
//
//  Created by 李松玉 on 15/3/16.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (LSY)
/**
 *  清空字符串中的空白字符
 *
 *  @return 清空空白字符串之后的字符串
 */
- (NSString *)trimString;

/**
 *  是否空字符串
 *
 *  @return 如果字符串为nil或者长度为0返回YES
 */
- (BOOL)isEmptyString;

/**
 *  返回沙盒中的文件路径
 *
 *  @return 返回当前字符串对应在沙盒中的完整文件路径
 */
- (NSString *)documentsPath;

/**
 *  写入系统偏好
 *
 *  @param key 写入键值
 */
- (void)saveToNSDefaultsWithKey:(NSString *)key;


/**
 *  时间戳转NSString
 *
 *  @param format   日期的格式
 *
 *  @return 返回根据format格式返回的NSString
 */
- (NSString *)dateSince1970:(NSString *)dateline format:(NSString *)format;

@end
