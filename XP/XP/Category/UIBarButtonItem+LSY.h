//
//  UIBarButtonItem+LSY.h
//  HLZ
//
//  Created by 李松玉 on 15/3/31.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (LSY)
+ (UIBarButtonItem *)itemWithTarget:(id)target action:(SEL)action image:(NSString *)image highImage:(NSString *)highImage;
@end
