//
//  UIImage+LSY.h
//  XP
//
//  Created by 李松玉 on 15/4/15.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (LSY)

/**
 *  放大图片
 */
-(UIImage*)scaleToSize:(CGSize)size;

@end
