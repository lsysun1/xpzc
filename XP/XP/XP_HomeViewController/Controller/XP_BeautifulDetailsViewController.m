//
//  XP_BeautifulDetailsViewController.m
//  XP
//
//  Created by 李松玉 on 15/4/16.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "XP_BeautifulDetailsViewController.h"

@interface XP_BeautifulDetailsViewController ()

@end

@implementation XP_BeautifulDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addBackBtn];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
