//
//  XP_HomeViewController.m
//  XP
//
//  Created by 李松玉 on 15/4/15.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "XP_HomeViewController.h"
#import "XP_ScrollView.h"
#import "XP_HomeCollectionViewCell.h"
#import "XP_BeautifulViewController.h"

#define MenuCellID @"menu"


@interface XP_HomeViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIScrollView *_scorllView;
    XP_ScrollView *_topScrollView;
    UICollectionView *_collectionView;
    NSArray *_menuIconArr;
    NSArray *_menuNameArr;
    UITableView *_tableView;

    
}
@end

@implementation XP_HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"首页";

    
    _scorllView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    _scorllView.backgroundColor = UIColorFromRGB(0xdcdcdc);
    _scorllView.scrollEnabled = YES;
    [self.view addSubview:_scorllView];
    
    [self setUpTopScrollView];
    [self setUpcollectionView];
    [self setUpTableView];
    
    _scorllView.contentSize = CGSizeMake(ScreenWidth,ScreenHeight * 2);

    
}



- (void) setUpTopScrollView
{
    _topScrollView = [[XP_ScrollView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 130)];
    _topScrollView.backgroundColor = [UIColor redColor];
    [_scorllView addSubview:_topScrollView];
}

- (void) setUpcollectionView
{
    
    _menuIconArr = @[@"sy_mlp",@"sy_qst",@"sy_fst",@"sy_mzx",@"sy_hf",@"sy_ms",@"sy_zx",@"sy_mr"];
    _menuNameArr = @[@"美丽拍",@"纤体瘦",@"服侍搭",@"美妆秀",@"护肤",@"养身",@"整形",@"美容"];
    
    // 1.流水布局
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    // 2.每个cell的尺寸
    layout.itemSize = CGSizeMake(70, 70);
    // 3.设置cell之间的水平间距
    layout.minimumInteritemSpacing = 0;
    // 4.设置cell之间的垂直间距
    layout.minimumLineSpacing = 10;
    // 5.设置四周的内边距
    layout.sectionInset = UIEdgeInsetsMake(layout.minimumLineSpacing, 20, 0, 20);
    
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_topScrollView.frame) + 2, ScreenWidth, 180) collectionViewLayout:layout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = [UIColor whiteColor];
    [_scorllView addSubview:_collectionView];
    
    // 注册cell(告诉collectionView将来创建怎样的cell)
    UINib *nib = [UINib nibWithNibName:@"XP_HomeCollectionViewCell" bundle:nil];
    [_collectionView registerNib:nib forCellWithReuseIdentifier:MenuCellID];
    

}

- (void) setUpTableView
{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_collectionView.frame) + 10, ScreenWidth, 400)];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    [_tableView setScrollEnabled:NO];
    [_scorllView addSubview:_tableView];
    _scorllView.contentSize = CGSizeMake(ScreenWidth, CGRectGetMinY(_tableView.frame) + _tableView.contentSize.height);
}




#pragma mark - _collectionView Delegate Method
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 8;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    XP_HomeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MenuCellID forIndexPath:indexPath];
    
    NSString *img = _menuIconArr[indexPath.row];
    NSString *name = _menuNameArr[indexPath.row];
    cell.iconView.image = [UIImage imageNamed:img];
    cell.nameView.text = name;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    XP_BeautifulViewController *beaVC = [[XP_BeautifulViewController alloc]init];
    NSString *name = _menuNameArr[indexPath.row];
    beaVC.title = name;
    [self.navigationController pushViewController:beaVC animated:YES];
}

#pragma mark - _tableView Delegate Method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    cell.textLabel.text = @"123123";
    return cell;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end