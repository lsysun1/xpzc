//
//  XP_BeautifulViewController.m
//  XP
//
//  Created by 李松玉 on 15/4/16.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "XP_BeautifulViewController.h"
#import "XP_BeautifulHeadView.h"
#import "XP_BeautifulCell.h"
#import "XP_BeautifulDetailsViewController.h"

@interface XP_BeautifulViewController () <UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
}
@end

@implementation XP_BeautifulViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addBackBtn];
    [self setUpTableView];
}


- (void) setUpTableView
{
    _tableView = [[UITableView alloc]init];
    _tableView.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.rowHeight = 80;
    [self.view addSubview:_tableView];
    
    
    XP_BeautifulHeadView *headView = [[XP_BeautifulHeadView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 150)];
    _tableView.tableHeaderView = headView;
    
}




#pragma mark - _tableView Delegate Method
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    XP_BeautifulCell *cell = [XP_BeautifulCell cellWithTableView:tableView];
    return cell;

}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 取消选中状态
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    XP_BeautifulDetailsViewController *detailVc = [[XP_BeautifulDetailsViewController alloc]init];
    [self.navigationController pushViewController:detailVc animated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
