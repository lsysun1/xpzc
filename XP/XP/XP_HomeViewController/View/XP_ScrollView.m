//
//  XP_ScrollView.m
//  XP
//
//  Created by 李松玉 on 15/4/15.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "XP_ScrollView.h"
const int ImageCount = 3;


@interface XP_ScrollView()<UIScrollViewDelegate>
{
    UIScrollView *_myScrollView;
    UIPageControl *_myPageController;
    NSTimer *_timer;
}
@end

@implementation XP_ScrollView

- (id)initWithFrame:(CGRect)frame
{
    if(self == [super initWithFrame:frame]){
        _myScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth , 130)];
        _myScrollView.delegate = self;
        _myScrollView.bounces = NO;
        _myScrollView.showsHorizontalScrollIndicator = NO;
        _myScrollView.showsVerticalScrollIndicator = NO;
        _myScrollView.pagingEnabled = YES;
        _myScrollView.contentSize = CGSizeMake(ImageCount * _myScrollView.bounds.size.width, 0);
        [self addSubview:_myScrollView];
        
        _myPageController = [[UIPageControl alloc]init];
        _myPageController.numberOfPages = ImageCount;
        
        // 控件尺寸
        CGSize size = [_myPageController sizeForNumberOfPages:ImageCount];
        
        _myPageController.bounds = CGRectMake(0, 10, size.width, size.height);
        _myPageController.center = CGPointMake(self.center.x, 130);
        
        
        [_myPageController addTarget:self action:@selector(pageChanged:) forControlEvents:UIControlEventValueChanged];
        [self addSubview:_myPageController];
        
        // 添加图片
        
        for (int index = 1; index <= ImageCount; index++) {
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:_myScrollView.bounds];
            imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"img_0%d.jpg", index]];
            
            [_myScrollView addSubview:imageView];
        }
        
        
        // 计算imageView的位置
        [_myScrollView.subviews enumerateObjectsUsingBlock:^(UIImageView *imageView, NSUInteger idx, BOOL *stop) {
            
            // 调整x => origin => frame
            CGRect frame = imageView.frame;
            frame.origin.x = idx * frame.size.width;
            
            imageView.frame = frame;
        }];
        //    NSLog(@"%@", self.scrollView.subviews);
        
        // 分页初始页数为0
        _myPageController.currentPage = 0;
        
        [self startTimer];
        
    }
    return self;
}

// 分页控件的监听方法
- (void)pageChanged:(UIPageControl *)page
{
    //    NSLog(@"%ld", (long)page.currentPage);
    
    // 根据页数，调整滚动视图中的图片位置 contentOffset
    CGFloat x = page.currentPage * _myScrollView.bounds.size.width;
    [_myScrollView setContentOffset:CGPointMake(x, 0) animated:YES];
}

- (void)startTimer
{
    _timer = [NSTimer timerWithTimeInterval:2.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
    // 添加到运行循环
    [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

- (void)updateTimer
{
    // 页号发生变化
    // (当前的页数 + 1) % 总页数
    int page = (_myPageController.currentPage + 1) % ImageCount;
    _myPageController.currentPage = page;
    
    //    NSLog(@"%ld", (long)myPageController.currentPage);
    // 调用监听方法，让滚动视图滚动
    [self pageChanged:_myPageController];
}

#pragma mark - ScrollView的代理方法
// 滚动视图停下来，修改页面控件的小点（页数）
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // 停下来的当前页数
    //    NSLog(@"%@", NSStringFromCGPoint(scrollView.contentOffset));
    
    // 计算页数
    int page = scrollView.contentOffset.x / scrollView.bounds.size.width;
    
    _myPageController.currentPage = page;
}

/**
 修改时钟所在的运行循环的模式后，抓不住图片
 
 解决方法：抓住图片时，停止时钟，送售后，开启时钟
 */
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //    NSLog(@"%s", __func__);
    // 停止时钟，停止之后就不能再使用，如果要启用时钟，需要重新实例化
    [_timer invalidate];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [self startTimer];
}
@end
