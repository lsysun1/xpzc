//
//  XP_BeautifulCell.m
//  XP
//
//  Created by 李松玉 on 15/4/16.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "XP_BeautifulCell.h"

@interface XP_BeautifulCell()
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *pinglunLabel;
@property (weak, nonatomic) IBOutlet UIImageView *jiantouView;

@end

@implementation XP_BeautifulCell

+(instancetype) cellWithTableView:(UITableView *)tableView
{
    static NSString *ID = @"BeautifulCell";
    XP_BeautifulCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if(cell == nil){
        //  从xib加载Cell
        cell = [[[NSBundle mainBundle] loadNibNamed:@"XP_BeautifulCell" owner:nil options:nil]lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}



- (void)awakeFromNib {
    // Initialization code
    self.jiantouView.frame = CGRectMake(ScreenWidth - 20, 30, 12, 21);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
