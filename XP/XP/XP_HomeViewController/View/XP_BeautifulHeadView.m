//
//  XP_BeautifulHeadView.m
//  XP
//
//  Created by 李松玉 on 15/4/16.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "XP_BeautifulHeadView.h"

@interface XP_BeautifulHeadView()
{
    UIImageView *_iconView;
    UILabel *_title;
    UILabel *_huati;
    UILabel *_renshu;
    UILabel *_content;
    UILabel *_bottomLine;
}
@end


@implementation XP_BeautifulHeadView



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        [self buildUI];
    }
    return self;
}

- (void) buildUI
{
    _iconView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 25, 100, 100)];
    _iconView.backgroundColor = [UIColor blackColor];
    [self addSubview:_iconView];
    
    _title = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_iconView.frame) + 15, 30, ScreenWidth - CGRectGetMaxX(_iconView.frame) -15 , 17)];
    _title.text = @"你好好好好";
    _title.font = [UIFont systemFontOfSize:17];
    [self addSubview:_title];
    
    _huati = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_iconView.frame) + 15, CGRectGetMaxY(_title.frame) + 10, 80, 13)];
    _huati.text = @"话题:1111";
    _huati.font = [UIFont systemFontOfSize:13];
    [self addSubview:_huati];
    
    _renshu = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_huati.frame) + 5, CGRectGetMaxY(_title.frame) + 10, 80, 13)];
    _renshu.text = @"人数:1111";
    _renshu.font = [UIFont systemFontOfSize:13];
    [self addSubview:_renshu];
    
    _content = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_iconView.frame) + 15, CGRectGetMaxY(_renshu.frame) + 10, ScreenWidth - CGRectGetMaxX(_iconView.frame) - 15 , 50)];
    _content.text = @"赠送酬勤可获得鱼丸，同时获得本直播间的VIP会员头衔。若直播时长达到，；";
    _content.font = [UIFont systemFontOfSize:13];
    _content.numberOfLines = 4;
    [self addSubview:_content];
    
    _bottomLine = [[UILabel alloc]initWithFrame:CGRectMake(5, CGRectGetMaxY(self.frame) - 1, ScreenWidth - 10, 1)];
    _bottomLine.backgroundColor = UIColorFromRGB(0xdcdcdc);
    [self addSubview:_bottomLine];

}


@end
