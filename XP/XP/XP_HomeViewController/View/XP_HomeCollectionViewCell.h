//
//  XP_HomeCollectionViewCell.h
//  XP
//
//  Created by 李松玉 on 15/4/15.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XP_HomeCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *nameView;

@end
