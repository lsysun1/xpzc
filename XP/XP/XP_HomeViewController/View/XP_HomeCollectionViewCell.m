//
//  XP_HomeCollectionViewCell.m
//  XP
//
//  Created by 李松玉 on 15/4/15.
//  Copyright (c) 2015年 李松玉. All rights reserved.
//

#import "XP_HomeCollectionViewCell.h"

@interface XP_HomeCollectionViewCell()

@end

@implementation XP_HomeCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
    self.nameView.textColor = UIColorFromRGB(0x7a7a7a); 
}

@end
